<?php
/*
Template Name: Full Width
*/
get_header(); ?>


<header id="featured-hero" role="banner">
  <div id="fire_title_container">
    <span class="menu-item"><a href="#home"><canvas id="fire_title_canvas" width="1600" height="200"></canvas></a></span>
    <img id="fire_ornaments" src="<?php echo get_template_directory_uri();?>/assets/images/fire-ornaments.svg" />
    <nav id="site-navigation" class="desktop-navigation" role="navigation">
      <div class="top-bar-menu">
        <?php TheMusician_top_bar_r(); ?>

        <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
          <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
        <?php endif; ?>
      </div>
    </nav>
  </div>
    <!-- <video
        id="fire_title"
        width="1200"
        height="200"
        src="<?php echo get_template_directory_uri();?>/assets/media/fire_title.mp4"
        poster="<?php echo get_template_directory_uri();?>/assets/images/fire-title.gif"
        autoplay
        loop
        ></video> -->
        <?php
    // Check the Customizer for featured images and generate Divs for each one
        $featured_img_count = 1;
        $featured_img;
        while($img_id = get_theme_mod( 'featured_img'.$featured_img_count )):
          ?>
        <div class="feature-img" id="feature-img-<?php echo $featured_img_count; ?>"  data-interchange="[<?php echo wp_get_attachment_image_src($img_id,'featured-small')[0]; ?>, small], [<?php echo wp_get_attachment_image_src($img_id,'featured-medium')[0]; ?>, medium], [<?php echo wp_get_attachment_image_src($img_id,'featured-large')[0]; ?>, large], [<?php echo wp_get_attachment_image_src($img_id,'featured-xlarge')[0]; ?>, xlarge]"></div>
        <?php
        $featured_img_count++;
        endwhile;
        ?>


      </header>
      <section id="music" class="page-content">
        <div class="row">
          <div class="columns small-12 medium-6">
            <h2>Latest Album</h2>
            <iframe style="border: 0; width: 100%; height: 450px;" src="https://bandcamp.com/EmbeddedPlayer/album=2193501859/size=large/bgcol=333333/linkcol=e32c14/tracklist=false/transparent=true/" seamless><a href="http://eveblackwater.bandcamp.com/album/stranger-danger">Stranger Danger by Eve Blackwater</a></iframe>
          </div>
          <div class="columns small-12 medium-6">
            <h2>Soundcloud Stream</h2>
            <iframe id="soundcloud-player" width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/9833849&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
          </div>
        </div>

      </section>
      <section id="shows" class="page-content">
        <script type='text/javascript' src='http://widget.bandsintown.com/javascripts/bit_widget.js'></script><a href="http://www.bandsintown.com/Charley%20Crockett" class="bit-widget-initializer" data-artist="Charley Crockett">Charley Crockett Tour Dates</a>
      </section>
      <section id="photos" class="page-content">

      </section>
      <section id="videos" class="page-content">
        <?php
            // Begin Portfolio Loop
        $video_query = new WP_Query(array( 
          'post_type' => array('video'), 
          'order'     => 'DESC',
          'orderby'   => 'meta_value',
          'posts_per_page'  => 20,
          'meta_key'  => 'video_date',
          'meta_type' => 'DATETIME'
          )
        );
        if($video_query->have_posts()): ?>
        <div class="orbit video-slider" role="region" aria-label="Videos" data-orbit data-auto-play="false">
          <ul class="orbit-container ease-out">
            <?php 
            while ( $video_query->have_posts() ) : $video_query->the_post(); ?>
            <li class="orbit-slide ease slow">
              <?php
              the_content();
              ?>
            </li>
          <?php endwhile; ?>
        </ul>
        <div class="orbit-nav">
          <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span> &lt;</button>
          <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&gt;</button>
        </div>
      </div>
    <?php endif; 
            // End Portfolio Loop 
    ?>
  </section>
  <section id="booking" class="page-content">

  </section>
  <?php get_footer();
