<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'TheMusician_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'TheMusician_layout_start' ); ?>

	<div class="title-bar" data-responsive-toggle="mobile-navigation" data-hide-for="medium up">
		<button class="menu-icon" type="button" data-toggle="mobile-menu"></button>
		<div class="title-bar-title">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Menu</a>
		</div>
	</div>
	<nav id="mobile-navigation" class="top-bar-menu" data-animate="mobile-transition-in mobile-transition-out">
	<?php TheMusician_top_bar_r(); ?>

	<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
		<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
	<?php endif; ?>
	</nav>
	
	<section id="home" class="container">
		<?php do_action( 'TheMusician_after_header' );
