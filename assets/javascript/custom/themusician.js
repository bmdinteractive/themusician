var active_feat_index = 0;
var prev_feat_index = 0;
var total_feat_img = 0;
$(document).ready(function () {
    // Initialize Front Page Slideshow
    initFeatureImgSlideshow();

	// Apply smooth scrolling to top menu links
	$(".menu-item a").on('click', function(event) {
		if (this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, 'swing', function(){
				window.location.hash = hash;
			});
	    } // End if
	});

	$(window).on("scroll", onScroll);
	
});

function onScroll(event){
	var scrollPos = $("body").scrollTop();
	var stickyEl = $("#fire_title_container");
	var stickTop = 520;
	$('.menu-item a').each(function () {
		var currLink = $(this);
		var refElement = $(currLink.attr("href"));
		if(refElement.offset()){
			var refTop = refElement.offset().top;
			var refBottom = (refElement.offset().top+refElement.height());

			if (scrollPos > (refTop-100) && scrollPos < refBottom && scrollPos > stickTop) {
				$('.menu-item a .btn').removeClass("active");
				if(fireText && $(this).text().length > 0 && currLink.attr('href') != '#home'){
					fireText.generateText($(this).text());
				}
			}
			else{
				$('.btn',currLink).removeClass("active");
			}
		}
	});	
	if(scrollPos > stickTop && !stickyEl.hasClass('sticky')){
		stickyEl.addClass('sticky');
	} else if(scrollPos < stickTop && stickyEl.hasClass('sticky')) {
		if(fireText){
			fireText.generateText("Eve Blackwater");
		}
		stickyEl.removeClass('sticky');
	}

}

function initFeatureImgSlideshow(){
	total_feat_img = $(".feature-img").length;
	$(".feature-img").each(function(index,el){
		$(el).css('z-index',total_feat_img-index);
	});
	active_feat_index = 1;
	featureImgAnimate();
}

function featureImgAnimate(){
	prev_feat_index = (active_feat_index == 1) ? total_feat_img : active_feat_index-1;
	var feat_img = $('#feature-img-'+active_feat_index);
	var prev_feat_img = $('#feature-img-'+prev_feat_index);
	$(".feature-img").each(function(index,el){
		if($(el) != feat_img || $(el) != prev_feat_img){
			$(el).css('z-index',-1);
		}
	});
	feat_img.css('z-index',1).addClass('scale-in');
	prev_feat_img.css('z-index',0).removeClass('scale-in');
	setTimeout(featureImgAnimate,13000);
	active_feat_index = (active_feat_index < total_feat_img) ? active_feat_index+1 : 1;
}
