<?php
/**
 * Template part for off canvas menu
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

?>

<nav class="off-canvas position-left" id="mobile-menu" data-off-canvas data-auto-focus="false" data-position="left" role="navigation">
  <?php TheMusician_mobile_nav(); ?>
</nav>

<div class="off-canvas-content" data-off-canvas-content>
