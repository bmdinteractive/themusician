<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
	<header id="featured-hero" role="banner">
		<?php 
		$featured_img_count = 1;
		$featured_img;
		while($img_id = get_theme_mod( 'featured_img'.$featured_img_count )):
			?>
		<div class="img-container"  data-interchange="[<?php echo wp_get_attachment_image_src($img_id,'featured-small')[0]; ?>, small], [<?php echo wp_get_attachment_image_src($img_id,'featured-medium')[0]; ?>, medium], [<?php echo wp_get_attachment_image_src($img_id,'featured-large')[0]; ?>, large], [<?php echo wp_get_attachment_image_src($img_id,'featured-xlarge')[0]; ?>, xlarge]"></div>
			<?php
			$featured_img_count++;
		endwhile;
		?>
		
	</header>
<?php endif;
