<?php
/**
 * Template part for off canvas menu
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

?>

<nav class="vertical menu" id="mobile-menu" role="navigation">
  <?php TheMusician_mobile_nav(); ?>
</nav>
