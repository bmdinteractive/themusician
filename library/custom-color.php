<?php
/**
 * Allow users to customize Typography
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

if ( ! function_exists( 'color_register_theme_customizer' ) ) :
function color_register_theme_customizer( $wp_customize ) {
	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'colors' , array(
		'title'	=> __('Colors','TheMusician'),
		'priority' => 1000,
	));
	
	// Set default for Title Font
	$wp_customize->add_setting(
		'background_color',
		array(
			'default'	=> '#000',
		)
	);
	$wp_customize->add_control( 
		new WP_Customize_Color_Control( 
			$wp_customize, 
			'background_color', 
			array(
			'label' => __( 'Background Color', 'TheMusician' ),
			'section' => 'colors',
			) 
		)
	);

}

add_action( 'customize_register', 'color_register_theme_customizer' );
endif;
