<?php
/**
 * Allow users to customize Images
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

if ( ! function_exists( 'images_register_theme_customizer' ) ) :
function images_register_theme_customizer( $wp_customize ) {
	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'featured_images' , array(
		'title'	=> __('Featured Images','TheMusician'),
		'priority' => 100,
	));
	
	// Set default for Feature Image 1
	$wp_customize->add_setting(
		'featured_img1',
		array(
			'default'	=> '',
		)
	);
	
	$wp_customize->add_control( 
		new WP_Customize_Media_Control( 
			$wp_customize, 
			'featured_img1', 
			array(
			'label' => __( 'Featured Image 1', 'TheMusician' ),
			'section' => 'featured_images',
			'mime_type' => 'image',
			) 
		)
	);
		
	// Set default for Feature Image 2
	$wp_customize->add_setting(
		'featured_img2',
		array(
			'default'	=> '',
		)
	);
	
	$wp_customize->add_control( 
		new WP_Customize_Media_Control( 
			$wp_customize, 
			'featured_img2', 
			array(
			'label' => __( 'Featured Image 2', 'TheMusician' ),
			'section' => 'featured_images',
			'mime_type' => 'image',
			) 
		)
	);
		
	// Set default for Feature Image 3
	$wp_customize->add_setting(
		'featured_img3',
		array(
			'default'	=> '',
		)
	);
	
	$wp_customize->add_control( 
		new WP_Customize_Media_Control( 
			$wp_customize, 
			'featured_img3', 
			array(
			'label' => __( 'Featured Image 3', 'TheMusician' ),
			'section' => 'featured_images',
			'mime_type' => 'image',
			) 
		)
	);
		
	// Set default for Feature Image 4
	$wp_customize->add_setting(
		'featured_img4',
		array(
			'default'	=> '',
		)
	);
	
	$wp_customize->add_control( 
		new WP_Customize_Media_Control( 
			$wp_customize, 
			'featured_img4', 
			array(
			'label' => __( 'Featured Image 4', 'TheMusician' ),
			'section' => 'featured_images',
			'mime_type' => 'image',
			) 
		)
	);
		
	// Set default for Feature Image 5
	$wp_customize->add_setting(
		'featured_img5',
		array(
			'default'	=> '',
		)
	);
	
	$wp_customize->add_control( 
		new WP_Customize_Media_Control( 
			$wp_customize, 
			'featured_img5', 
			array(
			'label' => __( 'Featured Image 5', 'TheMusician' ),
			'section' => 'featured_images',
			'mime_type' => 'image',
			) 
		)
	);

}

add_action( 'customize_register', 'images_register_theme_customizer' );
endif;
