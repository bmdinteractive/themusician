<?php
/**
 * Adds Custom posts for Videos
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */
function musician_register_video_posts() {  

	$video_args = array(  
	'label' => __('Videos'),  
	'singular_label' => __('Video'),  
	'public' => true,  
	'show_ui' => true,  
	'capability_type' => 'post',  
	'hierarchical' => true,  
	'rewrite' => true,  
	'has_archive' => true,
	'supports' => array('title', 'editor', 'thumbnail','tags')  
	);

	register_post_type( 'video' , $video_args );  
}
add_action('init', 'musician_register_video_posts');  
