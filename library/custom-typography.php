<?php
/**
 * Allow users to customize Typography
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

if ( ! function_exists( 'typography_register_theme_customizer' ) ) :
function typography_register_theme_customizer( $wp_customize ) {
	// Create custom field for mobile navigation layout
	$wp_customize->add_section( 'typography' , array(
		'title'	=> __('Typography','TheMusician'),
		'priority' => 1000,
	));
	
	// Set default for Title Font
	$wp_customize->add_setting(
		'title_font',
		array(
			'default'	=> __( 'dark', 'TheMusician' ),
		)
	);

	// Add options for Title Font Selector
	$wp_customize->add_control(
	    new WP_Customize_Control(
	        $wp_customize,
	        'title_font',
	        array(
	            'label'          => __( 'Title Font', 'title_font' ),
				'section' 	     => 'typography',
	            'settings'       => 'title_font',
	            'type'           => 'select',
	            'choices'        => array(
	                'dark'   => __( 'Dark' ),
	                'light'  => __( 'Light' )
	            )
	        )
	    )
	);

	// Set default for Title Font Size
	$wp_customize->add_setting(
		'title_font_size',
		array(
			'value'	=> 1,
		)
	);

	// Set default for Title Font Size
	$wp_customize->add_control( 'title_font_size', array(
	  'type' => 'range',
	  'section' => 'typography',
	  'label' => __( 'Title Font Size' ),
	  'description' => __( 'Set the title font size' ),
	  'input_attrs' => array(
	    'min' => 0,
	    'max' => 10,
	    'step' => 1,
	  ),
	) );	

	// Set default for Body Font
	$wp_customize->add_setting(
		'body_font',
		array(
			'default'	=> __( 'dark', 'TheMusician' ),
		)
	);

	// Add options for Body Font Selector	
	$wp_customize->add_control(
	    new WP_Customize_Control(
	        $wp_customize,
	        'body_font',
	        array(
	            'label'          => __( 'Body Font', 'body_font' ),
				'section' 	     => 'typography',
	            'settings'       => 'body_font',
	            'type'           => 'select',
	            'choices'        => array(
	                'dark'   => __( 'Dark' ),
	                'light'  => __( 'Light' )
	            )
	        )
	    )
	);
	// Set default for Body Font Size
	$wp_customize->add_setting(
		'body_font_size',
		array(
			'value'	=> 1,
		)
	);

	// Set default for Title Font Size
	$wp_customize->add_control( 'body_font_size', array(
	  'type' => 'range',
	  'section' => 'typography',
	  'label' => __( 'Body Font Size' ),
	  'description' => __( 'Set the body font size' ),
	  'input_attrs' => array(
	    'min' => 0,
	    'max' => 10,
	    'step' => 1,
	  ),
	) );	

}

add_action( 'customize_register', 'typography_register_theme_customizer' );
endif;
