<?php
/**
 * The sidebar containing the main widget area
 *
 * @package The Musician
 * @since The Musician 1.0.0
 */

?>
<aside class="sidebar">
	<?php do_action( 'TheMusician_before_sidebar' ); ?>
	<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
	<?php do_action( 'TheMusician_after_sidebar' ); ?>
</aside>
